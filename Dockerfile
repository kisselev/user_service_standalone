FROM maven:latest
COPY settings.xml /usr/share/maven/conf/settings.xml
ADD . /projects/user_service
WORKDIR /projects/user_service
CMD ["mvn", "spring-boot:run"]
