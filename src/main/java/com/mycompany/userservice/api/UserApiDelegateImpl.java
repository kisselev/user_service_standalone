package com.mycompany.userservice.api;

import com.mycompany.userservice.User;
import com.mycompany.userservice.UserMapper;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.mycompany.userservice.UserRepository;
import com.mycompany.userservice.api.model.UserDto;
import com.mycompany.userservice.api.UserApiDelegate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.badRequest;


@Service
public class UserApiDelegateImpl implements UserApiDelegate{

    private final UserRepository userRepository;

    public UserApiDelegateImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public ResponseEntity<UserDto> addUser(UserDto userDto) {
        try {
            User user = UserMapper.INSTANCE.mapTo(userDto);

            if (!user.phonenumber.matches("[+]?[0123456789 ]+")){
                System.out.println("Provided telephone number is invalid");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }

            if(user.getLastname().equals("")){
                System.out.println("Provided last name cannot be empty");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getLastname().length() < 2) {
                System.out.println("Provided last name is too short. Should be more than 1 letter long");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getLastname().length() > 32) {
                System.out.println("Provided last name is too long. Should be less than 32 letters long");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }


            if(user.getFirstname().equals("")) {
                System.out.println("Provided first name cannot be empty");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getFirstname().length() < 2) {
                System.out.println("Provided first name is too short. Should be more than 1 letter long");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getFirstname().length() > 32) {
                System.out.println("Provided first name is too long. Should be less than 32 letters long");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }

            if(user.getAddress().equals("")){
                System.out.println("Provided address cannot be empty");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getAddress().length() > 100){
                System.out.println("Provided address is too long");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }
            if(user.getAddress().length() < 5){
                System.out.println("Provided address is too short");
                return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            }

            // The user attributes have passed the checks. Proceeding by saving the user
            User returnedUser = this.userRepository.save(user);
            return new ResponseEntity<UserDto>(UserMapper.INSTANCE.mapTo(returnedUser), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
        }

    }

    @Override
    public ResponseEntity<UserDto> getUserById(
            @Parameter(name = "userId", description = "ID of user to return",
                    required = true, schema = @Schema(description = ""))
            @PathVariable("userId") Long userId) {

        // Convert Dto to warehouseProduct
        Optional<User> user = this.userRepository.findById(userId);
        return user.map(value -> ResponseEntity.ok(UserMapper.INSTANCE.mapTo(value))).orElseGet(() ->
                new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

//    @Override
//    public  ResponseEntity<UserDto> getUserByName(
//            @Parameter(name = "userId", description = "ID of user to return",
//                    required = true, schema = @Schema(description = ""))
//            @PathVariable("userId") Long userId) {
//        // Convert Dto to warehouseProduct
//        Optional<User> user = this.userRepository.findBy(userId);
//    }

    @Override
    public ResponseEntity<Void> deleteUser(Long userId, String apiKey) {
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isPresent()) {
            userRepository.deleteById(userId);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public ResponseEntity<Void> updateUser(Long userId, UserDto userDto) {
        Optional<User> user = this.userRepository.findById(userId);
        if (user.isPresent()) {
            userRepository.save(UserMapper.INSTANCE.mapTo(userDto));
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}