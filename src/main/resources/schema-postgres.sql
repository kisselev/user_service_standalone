DROP TABLE IF EXISTS warehouse_product;
DROP TABLE IF EXISTS user_table;

CREATE TABLE warehouse_product(id serial PRIMARY KEY, weight integer, location VARCHAR(255));

CREATE TABLE user_table(id serial PRIMARY KEY,
                        firstname VARCHAR(255),
                        lastname VARCHAR(255),
                        address VARCHAR(255),
                        phonenumber VARCHAR(255),
                        registrationdate VARCHAR(255),
                        paymentmethod VARCHAR(255)
                        );