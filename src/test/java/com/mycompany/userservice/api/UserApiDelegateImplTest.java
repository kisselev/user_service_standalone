package com.mycompany.userservice.api;

import com.mycompany.userservice.User;
import com.mycompany.userservice.UserService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.userservice.UserMapper;
import com.mycompany.userservice.api.model.UserDto;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = UserService.class)
class UserApiDelegateImplTest {

    @Autowired
    UserApiDelegateImpl userApiDelegate;

    public User initialize_user(String firstName,
                                String lastName,
                                String address,
                                String paymentMethod,
                                String phoneNumber,
                                String registrationDate) {
        User testUser = new User();
        testUser.setFirstname(firstName);
        testUser.setLastname(lastName);
        testUser.setAddress(address);
        testUser.setPaymentmethod(paymentMethod);
        testUser.setPhonenumber(phoneNumber);
        testUser.setRegistrationdate(registrationDate);
        return testUser;
    }

    @ParameterizedTest
    @CsvSource({"U,Kim,Technicka 2,cash,744722711,13/04/2022 15:45:00,422",
                "Jiri,Kim,Anglicka 2,online payment,24323424,01/05/2020 18:19:02,200"})
    void addUserTest(String firstName,
                              String lastName,
                              String address,
                              String paymentMethod,
                              String phoneNumber,
                              String registrationDate,
                              String expectedResponse) {
        System.out.println(lastName);
        User testUser = initialize_user(firstName,lastName,address,paymentMethod,phoneNumber,registrationDate);
        UserDto testUserDto = UserMapper.INSTANCE.mapTo(testUser);
        ResponseEntity<UserDto> response = userApiDelegate.addUser(testUserDto);
        assertEquals(Integer.parseInt(expectedResponse), response.getStatusCode().value());
    }

    @ParameterizedTest
    @CsvSource({"Jiri,Kim,Anglicka 2,online payment,24323424,01/05/2020 18:19:02"})
    void getUserByIdTest(String firstName,
                         String lastName,
                         String address,
                         String paymentMethod,
                         String phoneNumber,
                         String registrationDate) {
        User testUser = initialize_user(firstName,lastName,address,paymentMethod,phoneNumber,registrationDate);
        UserDto testUserDto = UserMapper.INSTANCE.mapTo(testUser);
        ResponseEntity<UserDto> added_user_response = userApiDelegate.addUser(testUserDto);

        Long userID = UserMapper.INSTANCE.mapTo(added_user_response.getBody()).getId();

        ResponseEntity<UserDto> response = userApiDelegate.getUserById(userID);
        UserDto userDto = response.getBody();
        User user = UserMapper.INSTANCE.mapTo(userDto);
        assertEquals("Jiri", user.getFirstname());
        assertEquals("Kim", user.getLastname());

    }

    @ParameterizedTest
    @CsvSource({"Jiri,Kim,Anglicka 2,online payment,24323424,01/05/2020 18:19:02"})
    void deleteUserTest(String firstName,
                    String lastName,
                    String address,
                    String paymentMethod,
                    String phoneNumber,
                    String registrationDate) {
        User testUser = initialize_user(firstName,lastName,address,paymentMethod,phoneNumber,registrationDate);
        UserDto testUserDto = UserMapper.INSTANCE.mapTo(testUser);
        ResponseEntity<UserDto> added_user_response = userApiDelegate.addUser(testUserDto);

        Long userID = UserMapper.INSTANCE.mapTo(added_user_response.getBody()).getId();

        ResponseEntity<Void> response = userApiDelegate.deleteUser(userID, "apiKey");
        assertEquals(200, response.getStatusCode().value());
    }

    @ParameterizedTest
    @CsvSource({"Vladyslav,Chytry,Anglicka 2,online payment,24323424,01/05/2020 18:19:02"})
    void updateUserTest(String firstName,
                        String lastName,
                        String address,
                        String paymentMethod,
                        String phoneNumber,
                        String registrationDate) {
        User testUser = initialize_user(firstName,lastName,address,paymentMethod,phoneNumber,registrationDate);
        UserDto testUserDto = UserMapper.INSTANCE.mapTo(testUser);
        ResponseEntity<UserDto> addedUserResponse = userApiDelegate.addUser(testUserDto);

        User returnedUser = UserMapper.INSTANCE.mapTo(addedUserResponse.getBody());

        returnedUser.setLastname("Hladchenko");
        returnedUser.setPaymentmethod("cash");

        ResponseEntity<Void> response =
                userApiDelegate.updateUser(returnedUser.getId(), UserMapper.INSTANCE.mapTo(returnedUser));

        assertEquals(200, response.getStatusCode().value());
    }
}