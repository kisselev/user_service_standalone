package com.mycompany.userservice.api.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.mycompany.userservice.api.model.ProductTypeDto;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * WarehouseProductDto
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-09-11T00:19:12.250494300+08:00[Asia/Shanghai]")
public class WarehouseProductDto   {

  @JsonProperty("id")
  private Long id;

  @JsonProperty("weight")
  private Integer weight;

  @JsonProperty("location")
  private String location;

  @JsonProperty("product")
  private ProductTypeDto product;

  public WarehouseProductDto id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public WarehouseProductDto weight(Integer weight) {
    this.weight = weight;
    return this;
  }

  /**
   * Get weight
   * @return weight
  */
  
  @Schema(name = "weight", required = false)
  public Integer getWeight() {
    return weight;
  }

  public void setWeight(Integer weight) {
    this.weight = weight;
  }

  public WarehouseProductDto location(String location) {
    this.location = location;
    return this;
  }

  /**
   * Get location
   * @return location
  */
  
  @Schema(name = "location", required = false)
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public WarehouseProductDto product(ProductTypeDto product) {
    this.product = product;
    return this;
  }

  /**
   * Get product
   * @return product
  */
  @Valid 
  @Schema(name = "product", required = false)
  public ProductTypeDto getProduct() {
    return product;
  }

  public void setProduct(ProductTypeDto product) {
    this.product = product;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WarehouseProductDto warehouseProduct = (WarehouseProductDto) o;
    return Objects.equals(this.id, warehouseProduct.id) &&
        Objects.equals(this.weight, warehouseProduct.weight) &&
        Objects.equals(this.location, warehouseProduct.location) &&
        Objects.equals(this.product, warehouseProduct.product);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, weight, location, product);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WarehouseProductDto {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    weight: ").append(toIndentedString(weight)).append("\n");
    sb.append("    location: ").append(toIndentedString(location)).append("\n");
    sb.append("    product: ").append(toIndentedString(product)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

