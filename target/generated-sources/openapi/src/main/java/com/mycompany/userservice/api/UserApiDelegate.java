package com.mycompany.userservice.api;

import com.mycompany.userservice.api.model.UserDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.annotation.Generated;

/**
 * A delegate to be called by the {@link UserApiController}}.
 * Implement this interface with a {@link org.springframework.stereotype.Service} annotated class.
 */
@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-09-11T00:19:12.250494300+08:00[Asia/Shanghai]")
public interface UserApiDelegate {

    default Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    /**
     * POST /user : Add a new user to the store
     * Add a new user to the store
     *
     * @param userDto Create a new user in the store (required)
     * @return successful operation (status code 200)
     *         or Invalid input (status code 405)
     * @see UserApi#addUser
     */
    default ResponseEntity<UserDto> addUser(UserDto userDto) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"firstname\" : \"firstname\", \"address\" : \"address\", \"phonenumber\" : \"phonenumber\", \"registrationdate\" : \"registrationdate\", \"id\" : 0, \"paymentmethod\" : \"paymentmethod\", \"lastname\" : \"lastname\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * DELETE /user/{userId} : Deletes a user
     *
     * @param userId User id to delete (required)
     * @param apiKey  (optional)
     * @return User is successfully deleted (status code 200)
     *         or Invalid ID supplied (status code 400)
     *         or User not found (status code 404)
     * @see UserApi#deleteUser
     */
    default ResponseEntity<Void> deleteUser(Long userId,
        String apiKey) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * GET /user/{userId} : Find user by ID
     * Returns a single user
     *
     * @param userId ID of user to return (required)
     * @return successful operation (status code 200)
     *         or Invalid ID supplied (status code 400)
     *         or User not found (status code 404)
     * @see UserApi#getUserById
     */
    default ResponseEntity<UserDto> getUserById(Long userId) {
        getRequest().ifPresent(request -> {
            for (MediaType mediaType: MediaType.parseMediaTypes(request.getHeader("Accept"))) {
                if (mediaType.isCompatibleWith(MediaType.valueOf("application/json"))) {
                    String exampleString = "{ \"firstname\" : \"firstname\", \"address\" : \"address\", \"phonenumber\" : \"phonenumber\", \"registrationdate\" : \"registrationdate\", \"id\" : 0, \"paymentmethod\" : \"paymentmethod\", \"lastname\" : \"lastname\" }";
                    ApiUtil.setExampleResponse(request, "application/json", exampleString);
                    break;
                }
            }
        });
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

    /**
     * PUT /user/{userId} : Update an existing user
     * Update an existing user by Id
     *
     * @param userId ID of user to return (required)
     * @param userDto Update an existent user in the store (required)
     * @return Successful operation (status code 200)
     *         or Invalid ID supplied (status code 400)
     *         or User not found (status code 404)
     *         or Validation exception (status code 405)
     * @see UserApi#updateUser
     */
    default ResponseEntity<Void> updateUser(Long userId,
        UserDto userDto) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);

    }

}
