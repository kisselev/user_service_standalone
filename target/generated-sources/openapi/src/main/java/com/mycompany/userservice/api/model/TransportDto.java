package com.mycompany.userservice.api.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.mycompany.userservice.api.model.OrderDto;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * TransportDto
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-09-11T00:19:12.250494300+08:00[Asia/Shanghai]")
public class TransportDto   {

  @JsonProperty("id")
  private Long id;

  @JsonProperty("dateOfDelivery")
  private String dateOfDelivery;

  @JsonProperty("finished")
  private Boolean finished;

  @JsonProperty("order")
  private OrderDto order;

  public TransportDto id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public TransportDto dateOfDelivery(String dateOfDelivery) {
    this.dateOfDelivery = dateOfDelivery;
    return this;
  }

  /**
   * Get dateOfDelivery
   * @return dateOfDelivery
  */
  
  @Schema(name = "dateOfDelivery", required = false)
  public String getDateOfDelivery() {
    return dateOfDelivery;
  }

  public void setDateOfDelivery(String dateOfDelivery) {
    this.dateOfDelivery = dateOfDelivery;
  }

  public TransportDto finished(Boolean finished) {
    this.finished = finished;
    return this;
  }

  /**
   * Get finished
   * @return finished
  */
  
  @Schema(name = "finished", required = false)
  public Boolean getFinished() {
    return finished;
  }

  public void setFinished(Boolean finished) {
    this.finished = finished;
  }

  public TransportDto order(OrderDto order) {
    this.order = order;
    return this;
  }

  /**
   * Get order
   * @return order
  */
  @Valid 
  @Schema(name = "order", required = false)
  public OrderDto getOrder() {
    return order;
  }

  public void setOrder(OrderDto order) {
    this.order = order;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TransportDto transport = (TransportDto) o;
    return Objects.equals(this.id, transport.id) &&
        Objects.equals(this.dateOfDelivery, transport.dateOfDelivery) &&
        Objects.equals(this.finished, transport.finished) &&
        Objects.equals(this.order, transport.order);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateOfDelivery, finished, order);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TransportDto {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dateOfDelivery: ").append(toIndentedString(dateOfDelivery)).append("\n");
    sb.append("    finished: ").append(toIndentedString(finished)).append("\n");
    sb.append("    order: ").append(toIndentedString(order)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

