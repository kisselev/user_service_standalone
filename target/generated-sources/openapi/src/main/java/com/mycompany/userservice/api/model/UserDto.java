package com.mycompany.userservice.api.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.openapitools.jackson.nullable.JsonNullable;
import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import javax.annotation.Generated;

/**
 * UserDto
 */

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-09-11T00:19:12.250494300+08:00[Asia/Shanghai]")
public class UserDto   {

  @JsonProperty("id")
  private Long id;

  @JsonProperty("firstname")
  private String firstname;

  @JsonProperty("lastname")
  private String lastname;

  @JsonProperty("address")
  private String address;

  @JsonProperty("phonenumber")
  private String phonenumber;

  @JsonProperty("registrationdate")
  private String registrationdate;

  @JsonProperty("paymentmethod")
  private String paymentmethod;

  public UserDto id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  */
  
  @Schema(name = "id", required = false)
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public UserDto firstname(String firstname) {
    this.firstname = firstname;
    return this;
  }

  /**
   * Get firstname
   * @return firstname
  */
  
  @Schema(name = "firstname", required = false)
  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public UserDto lastname(String lastname) {
    this.lastname = lastname;
    return this;
  }

  /**
   * Get lastname
   * @return lastname
  */
  
  @Schema(name = "lastname", required = false)
  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public UserDto address(String address) {
    this.address = address;
    return this;
  }

  /**
   * Get address
   * @return address
  */
  
  @Schema(name = "address", required = false)
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public UserDto phonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
    return this;
  }

  /**
   * Get phonenumber
   * @return phonenumber
  */
  
  @Schema(name = "phonenumber", required = false)
  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String phonenumber) {
    this.phonenumber = phonenumber;
  }

  public UserDto registrationdate(String registrationdate) {
    this.registrationdate = registrationdate;
    return this;
  }

  /**
   * Get registrationdate
   * @return registrationdate
  */
  
  @Schema(name = "registrationdate", required = false)
  public String getRegistrationdate() {
    return registrationdate;
  }

  public void setRegistrationdate(String registrationdate) {
    this.registrationdate = registrationdate;
  }

  public UserDto paymentmethod(String paymentmethod) {
    this.paymentmethod = paymentmethod;
    return this;
  }

  /**
   * Get paymentmethod
   * @return paymentmethod
  */
  
  @Schema(name = "paymentmethod", required = false)
  public String getPaymentmethod() {
    return paymentmethod;
  }

  public void setPaymentmethod(String paymentmethod) {
    this.paymentmethod = paymentmethod;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDto user = (UserDto) o;
    return Objects.equals(this.id, user.id) &&
        Objects.equals(this.firstname, user.firstname) &&
        Objects.equals(this.lastname, user.lastname) &&
        Objects.equals(this.address, user.address) &&
        Objects.equals(this.phonenumber, user.phonenumber) &&
        Objects.equals(this.registrationdate, user.registrationdate) &&
        Objects.equals(this.paymentmethod, user.paymentmethod);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, firstname, lastname, address, phonenumber, registrationdate, paymentmethod);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UserDto {\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    firstname: ").append(toIndentedString(firstname)).append("\n");
    sb.append("    lastname: ").append(toIndentedString(lastname)).append("\n");
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    phonenumber: ").append(toIndentedString(phonenumber)).append("\n");
    sb.append("    registrationdate: ").append(toIndentedString(registrationdate)).append("\n");
    sb.append("    paymentmethod: ").append(toIndentedString(paymentmethod)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

