package com.mycompany.userservice;

import com.mycompany.userservice.api.model.UserDto;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-09-11T00:19:14+0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
public class UserMapperImpl implements UserMapper {

    @Override
    public UserDto mapTo(User wp) {
        if ( wp == null ) {
            return null;
        }

        UserDto userDto = new UserDto();

        userDto.setId( wp.getId() );
        userDto.setFirstname( wp.getFirstname() );
        userDto.setLastname( wp.getLastname() );
        userDto.setAddress( wp.getAddress() );
        userDto.setPhonenumber( wp.getPhonenumber() );
        userDto.setRegistrationdate( wp.getRegistrationdate() );
        userDto.setPaymentmethod( wp.getPaymentmethod() );

        return userDto;
    }

    @Override
    public User mapTo(UserDto wpDto) {
        if ( wpDto == null ) {
            return null;
        }

        User user = new User();

        user.setId( wpDto.getId() );
        user.setFirstname( wpDto.getFirstname() );
        user.setLastname( wpDto.getLastname() );
        user.setAddress( wpDto.getAddress() );
        user.setPhonenumber( wpDto.getPhonenumber() );
        user.setRegistrationdate( wpDto.getRegistrationdate() );
        user.setPaymentmethod( wpDto.getPaymentmethod() );

        return user;
    }
}
