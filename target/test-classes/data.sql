DROP TABLE IF EXISTS user_table;

CREATE TABLE user_table(id serial PRIMARY KEY,
                        firstname VARCHAR(255),
                        lastname VARCHAR(255),
                        address VARCHAR(255),
                        phonenumber VARCHAR(255),
                        registrationdate VARCHAR(255),
                        paymentmethod VARCHAR(255)
);


INSERT INTO user_table(firstname, lastname, address, phonenumber, registrationdate, paymentmethod)
VALUES('Vlad', 'Hladchenko', 'Rumunska, 25', '334224098', '05/02/2022', 'online payment');

INSERT INTO user_table(firstname, lastname, address, phonenumber, registrationdate, paymentmethod)
VALUES('Jiri', 'Chytry', 'U Prikopu, 33/1', '534867295', '09/03/2020', 'cash');

INSERT INTO user_table(firstname, lastname, address, phonenumber, registrationdate, paymentmethod)
VALUES('David', 'Keplinger', 'Anglicka, 27', '788508848', '11/01/2021', 'cash');

INSERT INTO user_table(firstname, lastname, address, phonenumber, registrationdate, paymentmethod)
VALUES('Milan', 'Vu', 'Italska, 21', '774222193', '22/09/2018', 'online payment');